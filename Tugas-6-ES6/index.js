//Soal 1
const luasPP = (p, l) => {
    const luasPersegiPanjang = p * l
    return console.log(luasPersegiPanjang)
}
luasPP(4,4)

const kelilingPP = (p, l) => {
    const kelilingPersegiPanjang = 2 * (p+l)
    return console.log(kelilingPersegiPanjang)
}
kelilingPP(4,4)

//Soal 2
const newFunction = (firstName, lastName) => {
    const fullName = {firstName, lastName}
    return console.log(fullName)
}
//Driver Code 
newFunction("William", "Imoh")

//Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject
// Driver code
console.log(firstName, lastName, address, hobby)

//Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combinedArray = [...west, ...east]
//Driver Code
console.log(combinedArray)

//Soal 5
const planet = "earth" 
const view = "glass" 

const theString = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} `
console.log(theString)