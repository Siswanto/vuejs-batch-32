var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise 
const readBooks = async () => {
    let time = 10000;
    for (book of books) {
        await readBooksPromise(time, book).then(
            (sisaWaktu) => (time = sisaWaktu)
        );
    }
};

readBooks();