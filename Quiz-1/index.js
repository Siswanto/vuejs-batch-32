//Soal 1
function next_date (tanggal, bulan, tahun) {
  var day = new Date(`${tahun}-${bulan}-${tanggal}`)
  var nextDay = new Date(day)
  nextDay.setDate(day.getDate() + 1)
  return console.log(nextDay.toLocaleDateString('id-ID', {year: 'numeric', month: 'long', day: 'numeric'}))
}

// contoh 1
var tanggal = 29
var bulan = 2
var tahun = 2020
next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020
// contoh 2
var tanggal = 28
var bulan = 2
var tahun = 2021
next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021
// contoh 3
var tanggal = 31
var bulan = 12
var tahun = 2020
next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021

// ===========================================================================

//Soal 2
function jumlah_kata(kalimat)
{
  return console.log(kalimat.trim().split(' ').length)
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2
jumlah_kata(kalimat_3) // 4
